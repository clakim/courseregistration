/**
 * An Instructor is a specialized User that can teach a course. It is a superclass of FacultyMember.
 */

import java.util.ArrayList;

public abstract class Instructor extends User {

    private String department;
    private ArrayList<CourseSection> courseSectsTaught;

    public Instructor(String firstName, String lastName, int id, String email, String department){
        super(firstName, lastName, id, email);
        this.department = department;
    }

    public Instructor(String firstName, String lastName, int id, String email, String department, ArrayList<CourseSection> courseSectsTaught){
        super(firstName, lastName, id, email);
        this.department = department;
        this.courseSectsTaught = courseSectsTaught;
    }

    // Abstract method for approving student background
    public abstract boolean approveStuBackground(Student student, Course c);

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public ArrayList<CourseSection> getCourseSectsTaught() {
        return courseSectsTaught;
    }

    public void setCourseSectsTaught(ArrayList<CourseSection> courseSectsTaught) {
        this.courseSectsTaught = courseSectsTaught;
    }

    public abstract void sendEmail(String recipient, String text);

}
