/*
 * ClosedListingState is the state of a quarter between its (start date + 3 weeks) and end date.
 *
 * During this time:
 * a Course and its CourseSection are closed to Student enrollment,
 * a Course having less than 5 students is dropped
 * a Waitlist is wiped.
 */

public class ClosedListingState implements ListingState {

    public void handleEnroll(Course course) {

        System.out.println("In Closed Listing state");

        // If the course enrollment < 5...
        if (course.getCumulativeCount() <= 4) {
            course.setCatalogExclusion();  // this will update all "subscribers" to this course
        }
        // Wipe all waitlists
        Waitlist.wipe();
        course.setListingState(this);
    }

    public String toString() {
        return "Closed Listing State";
    }

}
