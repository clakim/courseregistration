/**
 * The Transcript class takes the grades from a Student, and is able to calculate the GPA.
 */

import java.util.HashMap;


public class Transcript {
    Student student;
    HashMap<Course,String> gradesMap;


    public Transcript(Student student) {
        this.student = student;
        gradesMap = new HashMap<Course,String>();
    }

    public void add(Course course, String grade) {
        gradesMap.put(course, grade);

    }

    public HashMap<Course,String> getGrades(){
        return gradesMap;
    }

    public double calculateGPA(){
        double sum = 0;
        for (String grade : gradesMap.values()) {
            Grade g = new Grade(grade);
            double numGrade = g.getNumGrade();
            sum += numGrade;
        }
        double gpa = sum/gradesMap.size();
        return gpa;
    }

}