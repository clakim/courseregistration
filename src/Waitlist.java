/**
 * Waitlist represents the roster of students that are eligible for enrollment in a CourseSection,
 * that have attempted enrollment without the result of being added.
 */

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Waitlist {

    //changed to Course from CourseSection
    private Course course;
    private ArrayList<Student> waitlistedStudents;  //list of enrolled students

    // Constructor
    public Waitlist(Course course) {
        waitlistedStudents = new ArrayList<>();
        this.course = course;
    }

    // List of students
    public ArrayList<Student> getWaitlistedStudents() {
        return this.waitlistedStudents;
    }

    public Course getCourse()
    {
        return course;
    }
    // Returns the next student in line for enrollment
    public Student getNextStudent() {
        Student luckyStudent = waitlistedStudents.get(0);
        waitlistedStudents.remove(waitlistedStudents.get(0));

        Statement deleteStmt = null;
        try {
            deleteStmt = DBConnection.getConnection().createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String deleteQuery = "DELETE FROM StudentsWaitlisted JOIN Waitlist WHERE Waitlist.courseID=" + course.getCourseID() +
                "AND StudentsWaitlisted.studentID = " + luckyStudent.getID() + ";";
        int i = 0;
        try {
            i = deleteStmt.executeUpdate(deleteQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (i == 1)
            System.out.println("Student removed from waitlist.");
        else
            System.out.println("No success");
        System.out.println(deleteQuery);
        return luckyStudent;
    }

    // Called when registration deadline has passed
    public static void wipe() {
        //...
        Statement deleteStmt = null;
        try {
            deleteStmt = DBConnection.getConnection().createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String deleteQuery = "DROP TABLE Waitlist;";
        int i = 0;
        try {
            i = deleteStmt.executeUpdate(deleteQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (i == 1)
            System.out.println("Waitlists wiped.");
        else
            System.out.println("No success");
        System.out.println(deleteQuery);
        System.out.println("Waitlist wiped for this course section");
    }

}