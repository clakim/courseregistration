/**
 * Student is a specialized User that can enroll in courses.
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Logger;

public class Student extends User implements Observer {

    private int year;
    private String major;
    private String enrollStatus;
    private int currentBalance;
    private ArrayList<Course> courseLoad;
    private int courseLimit;
    private Transcript transcript = new Transcript(this);

    private boolean hasBigFines;
    private EnrollmentState enrollmentState;

    final Logger logger = Logger.getLogger(Student.class.getName());

//    private boolean meetsCourseGradeReq;

    // Constructor contains parameters from the abstract superclass
    public Student(String firstName, String lastName, int id, String email){
        super(firstName, lastName, id, email);
        enrollmentState = null;         // is this correct????
    }

    public Student(String firstName, String lastName, int id, String email, int year, int currentBalance, String enrollStatus){
        super(firstName, lastName, id, email);
        this.year = year;
        this.currentBalance = currentBalance;
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.email = email;
        this.enrollStatus = enrollStatus;
        enrollmentState = null;
    }

        //Getter and setter methods

    public void setMajor(String major)
    {
        this.major = major;
    }

    public String getMajor()
    {
        return major;
    }

    // enrollStatus is "FT" or "PT"
    public void setEnrollStatus(String enrollStatus)
    {
        this.enrollStatus = enrollStatus;
    }

    public String getEnrollStatus()
    {
        return enrollStatus;
    }

    public void setCourseLimit(int courseLimit)
    {
        this.courseLimit = courseLimit;
    }

    public int getCourseLimit()
    {
        return courseLimit;
    }

    public int getCourseLoadSize() { return courseLoad.size(); }

    // Student's year (first year, second year, third year, fourth year)
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean hasbigFines(){return this.hasBigFines;}


    public boolean checkAdminRestriction()
    {
        // administrative restriction
        if (currentBalance < 0 && !hasBigFines)
        {
            return true;
        }
        return false;
    }

    // Check if can register for a certain course
    public boolean canRegisterCourse(Course course) throws SQLException {
        if ((getEnrollStatus()=="PT" || getEnrollStatus()=="FT") && getCourseLoadSize() <= getCourseLimit()) {
            if (checkAdminRestriction() == true && course.getInstructor().approveStuBackground(this, course) == true && (this.getGPA()>2.0)) {
                courseLoad.add(course);
                course.addToSection(this);

                System.out.println("Enrolled in " + course.toString() + "!");

                return true;
            }
            return false;
        }
        else

            return false;
    }

    public void sendEmail(String recipient, String txt)
    {
        logger.info("Mail Sent to " + recipient.toUpperCase() + ": " + txt);
    }

    // Add grade to transcript
    public void addGrade(Course course, String grade) {
        transcript.add(course, grade);
    }

    // Return calculated GPA from transcript
    public double getGPA()
    {
        return transcript.calculateGPA();
    }

    public Transcript getTranscript()
    {
        return transcript;
    }

    // Read all attributes of students in database
    public static void readAllStudents() throws SQLException {
        Statement stmt = DBConnection.getConnection().createStatement();
        String query1 = "SELECT * FROM Student JOIN User ON Student.studentID = User.userID";
        System.out.println(query1);
        ResultSet rs1 = stmt.executeQuery(query1);
        System.out.println("All Students:");
        while (rs1.next()) {
            String studentID = rs1.getString("studentID");
            String enrollStatus = rs1.getString("enrollStatus");
            String major = rs1.getString("major");
            String currentBalance = rs1.getString("currentBalance");
            String courseLoad = rs1.getString("courseLoad");
            String courseLimit = rs1.getString("courseLimit");
            String firstName = rs1.getString("firstName");
            String lastName = rs1.getString("lastName");
            String email = rs1.getString("email");
            System.out.println("First Name: " + firstName + ", Last Name: " + lastName + ", ID: "
                    + studentID + ", Enroll Status: " + enrollStatus + ",Major: " + major + ", Current Balance: " + currentBalance
                    + ", Course Load: " + courseLoad + ", Course Limit " + courseLimit + ", Email: " + email);
        }
    }

    // Read all attributes of student in database with certain ID
    public static void findStudentByID(int id) throws SQLException {
        Statement stmt = DBConnection.getConnection().createStatement();
        String query1 = "SELECT * FROM Student JOIN User ON Student.studentID = User.userID WHERE Student.studentID = " + id + "";
        System.out.println(query1);
        ResultSet rs1 = stmt.executeQuery(query1);
        System.out.println("Student by ID Query Results:");
        while (rs1.next()) {
            String firstName = rs1.getString("firstName");
            String lastName = rs1.getString("lastName");
            System.out.println("First Name: " + firstName + ", Last Name: " + lastName );
        }
    }

    // Insert rows in all pertinent tables for new Student addition
    public static void insertStudent(int id, String firstName, String lastName, String email, String enrollStatus,
                                     String major, float currentBalance, int courseLoadSize, int courseLimit, int passesAdminRestricts) throws SQLException {

        Statement insertStmt = DBConnection.getConnection().createStatement();
        String insertQuery = "INSERT into User " + "(userID, firstName, lastName, email) "
                + "VALUES (" + id + ",\"" + firstName + "\", \""
                + lastName + "\", " + "\"" + email + "\"" + ");";
        int i = insertStmt.executeUpdate(insertQuery);
        if (i==1)
            System.out.println("User recorded.");
        else
            System.out.println("No success");
        System.out.println(insertQuery);

        String insertQuery2 = "INSERT into Student " + "(studentID, enrollStatus, major, currentBalance, courseLoad, courseLimit, meetsAdminPrereq) "
                + "VALUES (" + id + "," + "\"" + enrollStatus + "\",\""
                + major + "\"," + currentBalance + "," + courseLoadSize + "," + courseLimit + "," + passesAdminRestricts + ")";
        int j = insertStmt.executeUpdate(insertQuery2);
        if (j==1)
            System.out.println("Student recorded.");
        else
            System.out.println("No success");
        System.out.println(insertQuery2);
    }

    // Insert rows in all pertinent tables for new Student addition
    public static void insertStudentToWaitlist(Course course, Student student) throws SQLException {
        Statement insertStmt = DBConnection.getConnection().createStatement();
        String insertQuery = "INSERT into StudentsWaitlisted " + "(waitlistID, studentID, timeStamp) "
                + "VALUES (waitlistID, " + student.getID() + ", " + LocalDateTime.now() + ") WHERE waitlistID in (SELECT * FROM StudentsWaitlisted JOIN Waitlist " +
                "ON Waitlist.waitlistID = StudentsWaitlisted.waitlistID AND Waitlist.courseID = " + course.getCourseID() + ";";
        int i = insertStmt.executeUpdate(insertQuery);
        if (i==1)
            System.out.println("Student added to waitlist.");
        else
            System.out.println("No success");
        System.out.println(insertQuery);
    }


    public static void insertTakesCourse(Course course, Student student) throws SQLException{
        Statement insertStmt = DBConnection.getConnection().createStatement();
        String insertQuery = "INSERT into TakesCourse " + "(studentID, courseID) "
                + "VALUES (" + student.getID() + ", " + course.getCourseID() + ");";
        int i = insertStmt.executeUpdate(insertQuery);
        if (i==1)
            System.out.println("Student added to TakesCourse list.");
        else
            System.out.println("No success");
        System.out.println(insertQuery);
    }


    public static void deleteStudent(int id) throws SQLException {

        Statement deleteStmt = DBConnection.getConnection().createStatement();
        String deleteQuery = "DELETE FROM Student WHERE studentID=" + id + ";";
        int i = deleteStmt.executeUpdate(deleteQuery);
        if (i == 1)
            System.out.println("Student deleted.");
        else
            System.out.println("No success");
        System.out.println(deleteQuery);
    }

    public void setCourseLoad(ArrayList<Course> courseLoad) {
        this.courseLoad = courseLoad;
    }

    public void setEnrollmentState(EnrollmentState enrollmentState) {
        this.enrollmentState = enrollmentState;
    }

    public EnrollmentState getEnrollmentState() {
        return enrollmentState;
    }

    public void update(String catalogRemovedNote, int courseID) {
        System.out.println("Email to student " + this.getID() + ": Course " + courseID + " is now "+ catalogRemovedNote);
    }
}