import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

/**
 * An Administrator is a specialized User that takes on the final responsibility of adding and dropping a course to the catalog.
 */

public class Administrator extends Staff {

    private CourseSection cs;
    private static Logger LOGGER = null;
    final Logger logger = Logger.getLogger(Student.class.getName());

    public Administrator(String firstName, String lastName, int id, String email, String employmentStatus){
        super(firstName, lastName, id, email, employmentStatus);

    }

    // createCourseSection method is called at the last step of adding a course to the catalog, after action from an Instructor
    public void createCourseSection(CourseSection cs) {
        if (cs.getCourseSectionSize() < 5)
        {
            cs.drop();

        }
        else
        {
            cs.addToCatalog();
            System.out.println("Course section size = " + cs.getCourseSectionSize());
        }
    }

    // Insert row in Course table for new course addition
    public static void insertCourse(int id, String courseName, String dept, int facultyID, int numCredits) throws SQLException {
        Statement insertStmt = DBConnection.getConnection().createStatement();
        String insertQuery = "INSERT into Course " + "(courseID, courseName, department, facultyID, numCredits) "
                + "VALUES (" + id + ",\"" + courseName + "\", \""
                + dept + "\", " + facultyID + "," + numCredits + ");";
        int i = insertStmt.executeUpdate(insertQuery);
        if (i == 1)
            System.out.println("Course recorded.");
        else
            System.out.println("No success");
        System.out.println(insertQuery);
    }


    // Update instructor teaching a course
    public static void updateCourseInstructor(int id, int facultyID) throws SQLException {
        Statement updateStmt = DBConnection.getConnection().createStatement();
        String updateQuery = "UPDATE Course SET facultyID = " + facultyID +
                " WHERE courseID = " + id + ";";
        int i = updateStmt.executeUpdate(updateQuery);
        if (i == 1)
            System.out.println("Course updated for instructor ID column.");
        else
            System.out.println("No success");
        System.out.println(updateQuery);
    }

    // Remove course from database
    public static void deleteCourse(int id) throws SQLException {

        Statement deleteStmt = DBConnection.getConnection().createStatement();
        String deleteQuery = "DELETE FROM Course WHERE courseID=" + id + ";";
        int i = deleteStmt.executeUpdate(deleteQuery);
        if (i == 1)
            System.out.println("Course deleted.");
        else
            System.out.println("No success");
        System.out.println(deleteQuery);
    }

    public void sendEmail(String recipient, String txt)
    {
        logger.info("Mail Sent to " + recipient.toUpperCase() + ": " + txt);
    }
}