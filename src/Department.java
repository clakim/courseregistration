/**
 * Every department has a department chair.
 * Minimal implementation for domain area
 */

public class Department {
    String name;
    DeptChair deptChair;

    public Department(String name){
        this.name = name;
    }

    public Department(String name, DeptChair deptChair){
        this.name = name;
        this.deptChair = deptChair;
    }

    public DeptChair getDeptChair() {
        return deptChair;
    }

    public void setDeptChair(DeptChair deptChair) {
        this.deptChair = deptChair;
    }

    @Override
    public String toString(){
        return name;
    }
}
