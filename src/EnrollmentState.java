import java.sql.SQLException;

public interface EnrollmentState {
    public void doAction(Student s, Course course) throws SQLException;
}
