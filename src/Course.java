/**
 * The Course class associates a course with instructor, department, and prerequisites.
 */

import java.sql.SQLException;
import java.util.ArrayList;

public class Course implements Subject {

    private int courseID;
    private String courseName;
    private Department dept;
    private Instructor instructor;
    private int numCredits;
    private ArrayList<Course> prereqs;
    private boolean needsInstrPermission;
    private ArrayList<CourseSection> sections = new ArrayList<CourseSection>();
    private ArrayList<Observer> observers = new ArrayList<>();
    private String catalogRemovedNote = "to be dropped";
    private ListingState listingState;

    public Course(int courseID, String courseName, Department dept, int numCredits) {
        this.courseID = courseID;
        this.courseName = courseName;
        this.dept = dept;
        this.numCredits = numCredits;
        listingState = null;
    }

    public Course(int courseID, String courseName, Department dept, Instructor instructor, int numCredits) {
        this.courseID = courseID;
        this.courseName = courseName;
        this.dept = dept;
        this.instructor = instructor;
        this.numCredits = numCredits;
    }

    public Course(int courseID, String courseName, Department dept, Instructor instructor, int numCredits, boolean needsInstrPermission, ArrayList<Course> prereqs) {
        this.courseID = courseID;
        this.courseName = courseName;
        this.dept = dept;
        this.instructor = instructor;
        this.numCredits = numCredits;
        this.needsInstrPermission = needsInstrPermission;
        this.prereqs = prereqs;
    }

    // set State pertaining to listedness in catalog
    public void setListingState(ListingState listingState) {
        this.listingState = listingState;
    }

    public ListingState getListingState() {
        return listingState;
    }

    public void createSections(int i, Quarter quarter){
        for (int h = 0; h < i; h++)
        { sections.add(new CourseSection(this, quarter));}
    }

    public void addToSection(Student student) throws SQLException {
        for (int i = 0; i < sections.size(); i++)
        {
            while (sections.get(i).getSeatsLeft() > 0) {
                sections.get(i).enroll(student); }
        }
    }

    // For each section in a course, get the total size
    public int getCumulativeCount(){
        int sum = 0;
        for (int i = 0; i < sections.size(); i++)
        {
            int x = sections.get(i).getCourseSectionSize();
            sum += x;
        }
        return sum;
    }

    // For each section in a course, get the total capacity
    public int getCumulativeCapacity(){
        int sum = 0;
        for (int i = 0; i < sections.size(); i++)
        {
            int x = sections.get(i).getCapacity();
            sum += x;
        }
        return sum;
    }

    //Getter and setter methods
    public Instructor getInstructor() {
        return this.instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public void setDept(Department dept) {
        this.dept = dept;
    }

    public Department getDept() {
        return dept;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String name) {
        this.courseName = name;
    }

    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    public ArrayList<Course> getPrereqs() {
        return prereqs;
    }

    public boolean reqsInstrPermission(){
        return needsInstrPermission;
    }

    @Override
    public String toString(){
        return "" + this.dept + " " + this.courseID + " " + this.courseName;
    }

    public int getNumCredits() {
        return numCredits;
    }

    public void setNumCredits(int numCredits) {
        this.numCredits = numCredits;
    }

    public void setSections(ArrayList<CourseSection> sections) {
        this.sections = sections;
    }

    public ArrayList<CourseSection> getSections() {
        return sections;
    }

    // Methods that make use of Observer pattern
    public ArrayList getObservers() {
        return observers;
    }

    public void setObservers(ArrayList observers) {
        this.observers = observers;
    }

    public void setCatalogExclusion() {
        notifyObservers();
    }

    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    public void notifyObservers() {
        System.out.println("Notifying to all interested parties (course instructor, registered students, academic advisors) " +
                "when course " + this.courseID + " is slated for drop");

        for (Observer ob : observers) {
                ob.update(catalogRemovedNote, this.courseID);
            }
        }
}