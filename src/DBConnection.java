/**
 * DBConnection
 * This class instantiates the database connection to be used by all classes in this package.
 */

import java.sql.*;

public class DBConnection {
    static Connection connection = null;
    static String connectionUrl = "jdbc:mysql://127.0.0.1:3306/reg_system?useSSL=false";
    static String connectionUser = "root";
    static String connectionPassword = "password";


    public static Connection getConnection()
    {
        if (connection != null) return connection;

        //return getConnection(db, user, pass);
        return getConnection(connectionUrl, connectionUser, connectionPassword);
    }

    private static Connection getConnection(String connectionUrl,String connectionUser, String connectionPassword)
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return connection;
    }
}