/*
 * Students having PendingEnrollmentState have just reached their courseLimit when seeking enrollment.
 * Can be put on a waitlist.
 */

public class PendingEnrollmentState implements EnrollmentState {
    public void doAction(Student s, Course course) {
        System.out.println("In Pending enrollment state");
        s.setEnrollmentState(this);

        // Send email requesting permission from department chair
        s.sendEmail("Department Chair " + course.getDept().getDeptChair().getLastName() + " " + course.getInstructor().getLastName()
                        + ", cc-ed to self and Academic Advisor", "Requesting permission for course " + course.getCourseID());

        // Department chair gives approval, so student gets added to the waitlist
        if (course.getDept().getDeptChair().approveStuBackground(s, course)) {
            s.setEnrollmentState(new TentativeEnrollmentState());
        }

        // Department chair does not give approval, so student reaches denied enrollment state
        else
            s.setEnrollmentState(new DeniedEnrollmentState());
    }

    public String toString() {
        return "Pending Enrollment State";
    }
}


