/**
 * A Department Chair is a specialized FacultyMember who can manage the addition of a Course to the catalog and decide a
 * student's enrollment and course section's creation by further criteria than a generic FacultyMember.
 */


import java.util.ArrayList;

public class DeptChair extends FacultyMember {

    private CourseSection cs;


    public DeptChair(String firstName, String lastName, int id, String email, String department, ArrayList<CourseSection> courseSections, String title) {
        super(firstName, lastName, id, email, department, courseSections, title);
    }

    // approveStuBackground method is called when department chair must approve background of part-time student taking 2 courses already
    @Override
    public boolean approveStuBackground(Student student, Course c) {
        if ((student.getEnrollStatus() == "PT") && student.getCourseLoadSize() == 2) {

            //Need to augment the logic; in some cases dept chair can approve
            if (student.getGPA() > 3.0){
                return true;
            }
            else
                return false;
        }
        return true;
    }

    // approveCourseSection method is called when department chair must approve of inclusion into catalog
    public void approveCourseSection(CourseSection cs) {
        if (cs.getCourseSectionSize() < 5) {
            cs.drop();
        } else {
            {
                cs.addToCatalog();
                System.out.println("Course section size = " + cs.getCourseSectionSize());
            }
        }
    }
}
