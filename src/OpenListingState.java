/*
 * OpenListingState is the state of a quarter between its start date and (start date + 3 weeks).
 *
 * During this time:
 * a Course and its CourseSection are open to Student enrollment,
 * a Waitlist is instantiated and fillable.
 */

import java.time.LocalDate;

public class OpenListingState implements ListingState {

    public void handleEnroll(Course course) {
        System.out.println("In Open Listing state");

    while (course.getSections().get(0).getQuarter().getDeadline().toLocalDate().isAfter(LocalDate.now())){

        //course and its sections enrollable
        //student is checked for prereq
        if (course.getCumulativeCapacity() >= course.getCumulativeCapacity()) {
            Waitlist waitlist = new Waitlist(course); //waitlist is instantiated and populable
            if (waitlist.getCourse().getCumulativeCount() < waitlist.getCourse().getCumulativeCapacity())  // && date is regDeadline + 1
            {
                waitlist.getNextStudent();
            }
        }
        course.setListingState(this);
    }

    // Out of while loop, deadline has passed
        course.setListingState(new ClosedListingState());
    }

    public String toString() {
        return "Open Listing State";
    }
}
