import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Each CourseSection has a Quarter. Deadline for registration is different for each Quarter instance.
 */

public class Quarter {

    private int id; // quarter is represented by unique numerical ID
    private String quarterName;
    private CourseSection cs;
    private Date regDeadline;


    public Quarter(int id, String quarterName, Date regDeadline){
        this.id = id;
        this.quarterName = quarterName;
        this.regDeadline = regDeadline;

    }

    // Each quarter has a deadline for registration.
    public Date getDeadline(){
        return regDeadline;
    }

    public void setDeadline(Date date){
        regDeadline = date;
    }


    // Insert row in Quarter table for new quarter addition
    public static void insertQuarter(int quarterID, String quarterName, String deadline) throws SQLException {

        Statement insertStmt = DBConnection.getConnection().createStatement();


        String insertQuery = "INSERT into Quarter " + "(quarterID, quarterName, deadline) VALUES (" + quarterID + ",\"" + quarterName + "\","
                + deadline + ");";
        System.out.println(insertQuery);

        int i = insertStmt.executeUpdate(insertQuery);
        if (i == 1)
            System.out.println("Quarter recorded.");
        else
            System.out.println("No success");
        System.out.println(insertQuery);
    }

    // Remove quarter from database
    public static void deleteQuarter(int id) throws SQLException {

        Statement deleteStmt = DBConnection.getConnection().createStatement();
        String deleteQuery = "DELETE FROM Quarter WHERE quarterID=" + id + ";";
        int i = deleteStmt.executeUpdate(deleteQuery);
        if (i == 1)
            System.out.println("Quarter deleted.");
        else
            System.out.println("No success");
        System.out.println(deleteQuery);
    }


}
