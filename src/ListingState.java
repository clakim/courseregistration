import java.sql.SQLException;

/*
 * State interface that embodies the listedness of a Course.
 */

public interface ListingState {
    public void handleEnroll(Course course) throws SQLException;
}
