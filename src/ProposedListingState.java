/*
 * ProposedListingState is the state of a quarter before its (start date - 2 weeks).
 *
 * During this time:
 * a Course and its CourseSections are proposable by a FacultyMember and addable to the course catalog.
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ProposedListingState implements ListingState {

    public void handleEnroll(Course course) throws SQLException {

        System.out.println("In Proposed Listing state");

        Statement stmt = DBConnection.getConnection().createStatement();
        String query1 = "SELECT * FROM User WHERE id = 1";      //get admin, who has been set to 1
        System.out.println(query1);
        ResultSet rs1 = stmt.executeQuery(query1);
        System.out.println("Find Admin Results:");
        while (rs1.next()) {
            String adminFirstName = rs1.getString("firstName");
            String adminLastName = rs1.getString("lastName");
            int id = Integer.parseInt(rs1.getString("id"));
            String email = rs1.getString("email");
            String employmentStatus = rs1.getString("employmentStatus");
            course.getInstructor().sendEmail(adminFirstName + " " + adminLastName, "Proposed Course: " + course.toString());
            try {
                Administrator admin = new Administrator(adminFirstName, adminLastName, id, email, employmentStatus);   //instantiating it for code sake
                admin.insertCourse(course.getCourseID(), course.getCourseName(), course.getDept().toString(), course.getInstructor().getID(), course.getNumCredits());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        course.setListingState(this);
    }

    public String toString() {
        return "Proposed Listing State";
    }
}
