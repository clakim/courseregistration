import java.sql.SQLException;
import java.sql.Statement;

/**
 * The Grade class maps the string representation with the numerical value of a grade.
 */

public class Grade {
    private String grade;

    public Grade(String grade) {
        this.grade = grade;
    }

    public double getNumGrade() {
        if (grade.equals("A"))
            return 4.0;
        if (grade.equals("A-"))
            return 3.7;
        if (grade.equals("B+"))
            return 3.3;
        if (grade.equals("B"))
            return 3.0;
        if (grade.equals("B-"))
            return 2.7;
        if (grade.equals("C+"))
            return 2.3;
        if (grade.equals("C"))
            return 2.0;
        if (grade.equals("C-"))
            return 1.7;
        if (grade.equals("D"))
            return 1.0;
        if (grade.equals("F"))
            return 0.0;
        if (grade.equals("W") || grade.equals("P") || grade.equals("F")){
            System.out.println("No numerical grade");} // need to handle such that 0 is not returned later
        return 0.0;
    }

    public String toString(){
        return grade;
    }

    // Insert row in Grade table for new grade addition
    public static void insertGrade(int studentID, int courseID, int sectionID, int quarterID, String grade) throws SQLException {

        Statement insertStmt = DBConnection.getConnection().createStatement();
        String insertQuery = "INSERT into Grade " + "(studentID, courseID, sectionID, quarterID, grade) "
                + "VALUES (" + studentID + "," + courseID + "," + sectionID + "," + quarterID + ",\""
                + grade + "\");";
        int i = insertStmt.executeUpdate(insertQuery);
        if (i == 1)
            System.out.println("Grade recorded.");
        else
            System.out.println("No success");
        System.out.println(insertQuery);
    }

    // Remove row from Grade table for  grade deletion
    public static void deleteGrade(int studentID, int courseID, int quarterID) throws SQLException {

        Statement deleteStmt = DBConnection.getConnection().createStatement();
        String deleteQuery = "DELETE from Grade WHERE studentID=" + studentID + " AND courseID=" + courseID + " AND quarterID=" + quarterID + ";";
        int i = deleteStmt.executeUpdate(deleteQuery);
        if (i == 1)
            System.out.println("Grade deleted.");
        else
            System.out.println("No success");
        System.out.println(deleteQuery);
    }

    // Update grade
    public static void updateGrade(int studentID, int courseID, int quarterID, String grade) throws SQLException {
        Statement updateStmt = DBConnection.getConnection().createStatement();
        String updateQuery = "UPDATE Grade SET grade =" + "\"" + grade + "\"" +
                " WHERE studentID=" + studentID + " AND courseID=" + courseID + " AND quarterID=" + quarterID + ";";
        int i = updateStmt.executeUpdate(updateQuery);
        if (i == 1)
            System.out.println("Grade updated.");
        else
            System.out.println("No success");
        System.out.println(updateQuery);
    }


}
