/**
 * A FacultyMember is a specialized Instructor that can assign grades and decide a student's enrollment based on prerequisite fulfillment
 */

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Logger;

public class FacultyMember extends Instructor implements Observer{

    private String title;
    final Logger logger = Logger.getLogger(Student.class.getName());

    // Each faculty member has a title and array list of sections taught (which may be empty)
    public FacultyMember(String firstName, String lastName, int id, String email, String department, ArrayList<CourseSection> courseSections, String title){
        super(firstName, lastName, id, email, department, courseSections);
        this.title = title;
    }

    // Assign grade for a student
    public void assignGrade(CourseSection cs, Student student, String grade) {
        cs.setGrade(student, grade);
    }

    // Approve background based on prerequisites-taking
    public boolean approveStuBackground(Student student, Course c)
    {
        if (student.getTranscript().getGrades().keySet().containsAll(c.getPrereqs())){
            return true;
        }
        return false;
    }

    public static void insertFaculty(int id, String firstName, String lastName, String email,
                                     String departmentTaught, String sectionTaught, String title) throws SQLException {

        Statement insertStmt = DBConnection.getConnection().createStatement();
        String insertQuery = "INSERT into User " + "(userID, firstName, lastName, email) "
                + "VALUES (" + id + ",\"" + firstName + "\", \""
                + lastName + "\", " + "\"" + email + "\"" + ");";
        int i = insertStmt.executeUpdate(insertQuery);
        if (i==1)
            System.out.println("User recorded.");
        else
            System.out.println("No success");
        System.out.println(insertQuery);

        String insertQuery2 = "INSERT into Instructor " + "(instructorID, department, sectionTaught) "
                + "VALUES (" + id + ",\"" + departmentTaught + "\", \""
                + sectionTaught + "\"" + ");";
        int j = insertStmt.executeUpdate(insertQuery2);
        if (j==1)
            System.out.println("Instructor recorded.");
        else
            System.out.println("No success");
        System.out.println(insertQuery2);

        String insertQuery3 = "INSERT into Faculty " + "(facultyID, title) "
                + "VALUES (" + id + "," + "\"" + title + "\"" + ")";
        int k = insertStmt.executeUpdate(insertQuery3);
        if (k==1)
            System.out.println("Faculty recorded.");
        else
            System.out.println("No success");
        System.out.println(insertQuery3);
    }

    // Remove faculty from database
    public static void deleteFaculty(int id) throws SQLException {

        Statement deleteStmt = DBConnection.getConnection().createStatement();
        String deleteQuery = "DELETE FROM Faculty WHERE facultyID=" + id + ";";
        int i = deleteStmt.executeUpdate(deleteQuery);
        if (i == 1)
            System.out.println("Faculty member deleted.");
        else
            System.out.println("No success");
        System.out.println(deleteQuery);
    }

    public void update(String catalogInclusion, int courseID) {
        System.out.println("Email to faculty member " + this.getID() + ": Course " + courseID + " is now "+ catalogInclusion);
    }

    public void sendEmail(String recipient, String txt)
    {
        logger.info("Mail Sent to " + recipient.toUpperCase() + ": " + txt);
    }

}