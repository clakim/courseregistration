/**
 * DBConnectionTest
 * Tests MYSQL read, insert, update, delete queries specified by individual classes to the database.
 */

import java.sql.SQLException;
import java.text.ParseException;

public class DBConnectionTest {

    public static void main(String[] args) throws SQLException, ParseException {

        try {
//            Student.readAllStudents();
//            Student.findStudentByID(1);
//            Student.insertStudent(20,"Gracie","Thompson","niece@hotmail.com","FT","MUSI",0,3,4,1)
//            Student.deleteStudent(20);
//
//            FacultyMember.insertFaculty(21, "Glenn", "Gould", "lovebach@yahoo.com",
//                    "MUSI", "", "Emeritus");
//            FacultyMember.deleteFaculty(21);

            //           Course.insertCourse(301,"Onomatopoeia in Ornithological Music","MUSI",13,3);
//            Course.updateCourseInstructor(301, 12);
//            Course.deleteCourse(301);

//            Course c = new Course(303,"The Semantics of Prison French","FREN", 3);  //another means of query, resulting from class instantiation
//            c.insertCourseAfterInstantiation();
//            Course.deleteCourse(303);

//            Grade.insertGrade(1,101,1,1802,"F");
//            Grade.updateGrade(1,101,1802,"C");
//            Grade.deleteGrade(1,101,1802);
            Quarter.insertQuarter(1803,"Summer 2018", "20180626");
            //           Quarter.deleteQuarter(1803);


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
