import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/*
 * Driver program demonstrates system's ability to prescribe permission and send notification of course
 * enrollability and student enrollability.
 *
 * Roles that the tester may try in this driver: Administrator, Student, FacultyMember.
 */
public class ProgramRunner {
    static Scanner in = new Scanner(System.in);
    static Statement initialStmt;

    // Launch the main menu
    public static void main(String[] args) throws IOException, SQLException {
        mainMenu();
    }

    // Main menu where you can choose your role
    public static void mainMenu() throws IOException, SQLException {
            System.out.println("Press (S) for Student, (F) for Faculty, (A) for Admin access, or (Q) to quit");
            String input = in.next();
            if(input.equalsIgnoreCase("s")) {
                studentEntry();
                mainMenu();
            }
            else if(input.equalsIgnoreCase("f")) {
                facultyEntry();
                mainMenu();
            }
            else if(input.equalsIgnoreCase("a")) {
                adminEntry();
                mainMenu();
            }

            else if(input.equalsIgnoreCase("q")) {
                System.out.println("Goodbye!");
                System.exit(1);
            }
            else {
                System.out.println("Invalid key input");
                mainMenu();
            }
    }


    // Student login
    public static void studentEntry() throws IOException, SQLException {
        try
        {
            Statement initialStmt = DBConnection.getConnection().createStatement();

            System.out.println("Student Login Portal");
            System.out.println("Enter your ID number (2-35): ");
            String userID = in.next();

            // Finds user with database table query
            ResultSet rs = initialStmt.executeQuery("SELECT * FROM UserPassword where userID =" + userID);

            while(rs.next())
            {
                System.out.println("Password: ");
                String password = in.next();

                // Authenticates user based on database table
                if(userID.equals(rs.getString("userID")) && password.equals(rs.getString("userPassword"))
                        && rs.getString("userLoginCategory").equals("student"))
                {
                    System.out.println("Hello, Student " + userID);
                    System.out.println("Imagine you are trying to enroll in a course. Choose from one of the following scenarios:");
                    System.out.println("(1) All enrollment criteria fulfilled (prerequisites, space, timing)");
                    System.out.println("(2) Course is already at capacity");
                    System.out.println("(3) Course requires instructor permission");
                    System.out.println("(4) You have already reached your courseload limit");
                    System.out.println("(5) Course accepts you, but the roster will inevitably have < 5 students.");
                    System.out.println("(6) The registration deadline has passed");
                    System.out.println("(7) Course has a prerequisite course that you may/may not have taken");
                    System.out.println("(8) Quit");
                    int keyPressed = in.nextInt();

                    // Enrollment scenario when all enrollment criteria fulfilled (prerequisites, space, timing)
                    if(keyPressed == 1) {
                        System.out.println("Enter desired courseID");
                        int courseID = in.nextInt();

                        /* essentially carries out course.addToSection(student) method; */
                        String update = "INSERT INTO TakesCourse VALUES (" + userID + "," + courseID + "," + 1801 + ")";
                        System.out.println(update);
                        initialStmt.executeUpdate(update);    //1802 hard-coded as 2018 winter quarter for example's sake
                        System.out.println("Successful enrollment: all criteria fulfilled");
                        return;
                    }

                    // Course is already at capacity
                    else if(keyPressed == 2) {
                        //System.out.println("Enter desired courseID");
                        //int courseID = in.nextInt();  //always same number as waitlistID

                        //Instead of above, hard-coded class that is at capacity in database for example's sake
                        int courseID = 1;
                         /* essentially carries out s.setState(new TentativeEnrollmentState()); */
                        String update = "INSERT INTO StudentsWaitlisted VALUES (" +
                                courseID + "," + Integer.parseInt(userID) + "," + "2018-10-15 14:19:24" + ")";
                        System.out.println(update);
                        initialStmt.executeUpdate(update);
                        System.out.println("E-mail sent to instructor; added to waitlist");
                        return;

                    }

                    // Course requires instructor permission
                    else if(keyPressed == 3) {
                        //System.out.println("Enter desired courseID");
                        //int courseID = in.nextInt();  //always same number as waitlistID

                        //Hard-coded class that requires instructor permission, incidentally same as above
                        int courseID = 1;

                        String insert = "INSERT INTO StudentsWaitlisted VALUES (" + courseID + "," + Integer.parseInt(userID) + ",'2018-11-11 12:12:12')";
                        System.out.println(insert);
                        /* again carries out s.setState(new TentativeEnrollmentState()); */
                        initialStmt.executeUpdate(insert);
                        System.out.println("E-mail sent; request brought to instructor's attention");
                        return;
                    }

                    // Student's courseload limit already reached
                    else if(keyPressed == 4) {
                         /* essentially carries out s.setState(new PendingEnrollmentState()); */
                        System.out.println("E-mail sent to department chair for request review");
                        return;
                    }

                    // Course accepts student, but the roster will inevitably have < 5 students
                    else if(keyPressed == 5) {
                        System.out.println("Enter desired courseID");
                        int courseID = in.nextInt();  //always same number as waitlist
                         /* c.setState(new ClosedListingState()); */
                        Statement deleteStmt = DBConnection.getConnection().createStatement();
                        String deleteQuery = "DELETE FROM Course WHERE courseID=" + courseID + ";";
                        int i = deleteStmt.executeUpdate(deleteQuery);
                        if (i == 1)
                            System.out.println("Course deleted.");
                        else
                            System.out.println("No success");
                        System.out.println(deleteQuery);
                        return;
                    }

                    // The registration deadline has passed
                    else if(keyPressed == 6) {
                        System.out.println(new ClosedListingState().toString());
                        System.out.println("Cannot enroll, deadline has passed");
                        return;
                    }

                    // Course has a prerequisite course that student may/may not have taken
                    else if(keyPressed == 7) {
                        System.out.println("Enter desired courseID");
                        int courseID = in.nextInt();
                        rs = initialStmt.executeQuery("SELECT * from Course where courseID =" + courseID);

                        String prereqCourseID = null;
                        while(rs.next()) {
                            prereqCourseID = rs.getString("prereqCourseID");
                        }
                        System.out.println("Prereq course ID: " + prereqCourseID);
                        rs.close();

                        String query = "SELECT *  from Transcript where transcriptID = " + userID + " AND courseID = " + prereqCourseID;
                        System.out.println(query);
                        rs = initialStmt.executeQuery(query);
                        boolean hasTakenPrerequisite = false;
                        while(rs.next())
                            { hasTakenPrerequisite = true;}
                            rs.close();
                        if (hasTakenPrerequisite){
                            initialStmt.executeUpdate("INSERT INTO TakesCourse VALUES (" +
                                    Integer.parseInt(userID) + "," + courseID + "," + 1801 + ")");
                            System.out.println("Successful enrollment");
                        }
                        return;
                        }

                    // Exit
                    else if(keyPressed == 8) {
                        System.out.println("Exit");
                        return; }

                    else {
                        System.out.println("Invalid key input, must be number 1-8");
                        studentEntry();}
                }
                else {
                    System.out.println("Login failed!");
                    mainMenu();
                }
            }
        }
        catch(SQLException e)
        {
            System.out.println("Error in SQL Query!");
            System.err.println(e);
        }
    }

    // Faculty login
    public static void facultyEntry() throws IOException
    {
        try
        {
            Statement initialStmt = DBConnection.getConnection().createStatement();
            Statement stmt1 = DBConnection.getConnection().createStatement();

            System.out.println("Faculty Login Portal");
            System.out.println("Enter your ID number (36-44): ");
            String userID = in.next();

            // Finds user with database table query
            ResultSet rs = initialStmt.executeQuery("SELECT * FROM UserPassword where userID =" + Integer.parseInt(userID));
            while(rs.next())
            {
                System.out.println("Password: ");
                String password = in.next();

                // Authenticates user based on database table
                if(userID.equals(rs.getString("userID")) && password.equals(rs.getString("userPassword"))
                        && rs.getString("userLoginCategory").equals("faculty"))
                {
                    System.out.println("Hello, Faculty " + userID);
                    System.out.println("Choose from one of the following course enrollment and permissions scenarios:");
                    System.out.println("(1) See the waitlisted students for your course");
                    System.out.println("(2) Add Tentative-Enrollment student to course, if applicable");
                    System.out.println("(3) Give a student in your class an F (may affect his/her registrability for the next quarter");
                    System.out.println("(4) Quit");
                    int keyPressed = in.nextInt();

                    // Review all waitlisted students
                    if(keyPressed == 1) {
                        System.out.println("Enter your courseID");
                        int courseID = in.nextInt();

                        rs = initialStmt.executeQuery("SELECT * from StudentsWaitlisted where waitlistID =" + courseID);
                        while(rs.next()) {
                            String studentID = rs.getString("studentID");
                            System.out.println("studentID: "+ studentID);
                        }
                        rs.close();
                        return;
                    }

                    // Add Tentative-Enrollment student to course roster
                    else if(keyPressed == 2) {
                        System.out.println("Enter your courseID");
                        int courseID = in.nextInt();
                        int studentID = 3; //set 3 for illustration  but student really pushed off of ArrayList
                        /* carries out s.setState(new EnrolledEnrollmentState()); */
                        initialStmt.executeUpdate("INSERT INTO TakesCourse VALUES (" +
                                studentID + "," + courseID + "," + 1801 + ")");
                        System.out.println(new EnrolledEnrollmentState().toString());
                        return;
                    }

                    // Give a student in your class a grade
                    else if(keyPressed == 3) {
                        System.out.println("Enter the student ID");
                        String studentID = in.next();
                        System.out.println("Enter the course ID");
                        String courseID = in.next();
                        System.out.println("Enter the quarter ID");
                        String quarterID = in.next();
                        System.out.println("Enter the grade");
                        String grade  = in.next();
                        /* from faculty "assignGrade(CourseSection cs, Student student, String grade)" */
                        String insert = "INSERT INTO Transcript VALUES (" +
                                Integer.parseInt(studentID) + "," + Integer.parseInt(courseID) + "," + Integer.parseInt(quarterID) + "," + "'" + grade + "')";
                        System.out.println(insert);
                        initialStmt.executeUpdate(insert);

                        rs.close();
                        return;
                    }
                    else if(keyPressed == 4) {
                        System.out.println("Exit");
                        return;
                    }


                    else {
                        System.out.println("Invalid key input, must be number 1-4");
                        facultyEntry();}
                    rs.close();
                }
                else {
                    System.out.println("Login failed!");
                    mainMenu();
                }
            }
        }
        catch(SQLException e)
        {
            System.out.println("Error in SQL Query!");
            System.err.println(e);
            e.printStackTrace();
        }
    }


    // Admin login
    public static void adminEntry() throws IOException
    {
        try
        {
            Statement initialStmt = DBConnection.getConnection().createStatement();
            Statement stmt1 = DBConnection.getConnection().createStatement();

            System.out.println("Administrator Login Portal");
            System.out.println("Enter your ID number (1): ");
            String userID = in.next();

            // Finds user with database table query
            ResultSet rs = initialStmt.executeQuery("SELECT * FROM UserPassword where userID =" + Integer.parseInt(userID) + "");
            while(rs.next())
            {
                System.out.println("Password: ");
                String password = in.next();

                // Authenticates user based on database table
                if(userID.equals(rs.getString("userID")) && password.equals(rs.getString("userPassword"))
                        && rs.getString("userLoginCategory").equals("admin"))
                {
                    System.out.println("Hello, Administrator");
                    System.out.println("Choose from one of the following scenarios:");
                    System.out.println("(1) Examine courses with less than 5 students");
                    System.out.println("(2) Manually update the prerequisites of a course");
                    System.out.println("(3) Quit");

                    int keyPressed = in.nextInt();
                    if(keyPressed == 1) {

                        rs = stmt1.executeQuery("SELECT * from CourseSection cs JOIN Course ON cs.courseID = Course.courseID " +
                                "where cs.capacity-cs.seatsLeft <= 4");
                        while(rs.next())
                        {
                            String courseID = rs.getString("courseID");
                            System.out.println("courseID: "+ courseID);
                        }
                        /* course.setCatalogExclusion(); then launches the subject-observer behavior */
                    }


                    else if(keyPressed==2) {
                        System.out.println("Enter courseID to lift instructor permission prerequisite from");
                        int courseID = in.nextInt();
                        String update = "UPDATE Course SET needsInstrPerm = 0 where needsInstrPerm = 1";
                        System.out.println(update);
                        initialStmt.executeUpdate(update);
                        System.out.println("Lifted instructor permission prereq from Course " + courseID);
                        return;
                    }

                    else if(keyPressed == 3) {
                        System.out.println("Exit");
                        return;
                    }
                    else {
                        System.out.println("Invalid key input, must be 1-3");
                        adminEntry();
                    }
                }
                else {
                    System.out.println("Login failure; incorrect userID or password");
                    adminEntry();
                }
            }
        }
        catch(SQLException e)
        {
            System.out.println("Error in SQL Query!");
            System.err.println(e);
            e.printStackTrace();
        }
    }
}