// The final state in the sequential path of EnrollmentStates for a Student.

public class DeniedEnrollmentState implements EnrollmentState {

    public void doAction(Student s, Course course) {

        System.out.println("In Denied enrollment state");
        s.setEnrollmentState(this);
        course.getInstructor().sendEmail("Student " + s.getID() + ": ", "Enrollment denied" );
}
    public String toString() {
        return "Denied Enrollment State";
    }
}
