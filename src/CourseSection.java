/**
 * A CourseSection is a section of a Course that is involved with the filling of seats, inclusion into the catalog,
 * and association with a student's grade in the course.
 */

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Logger;

public class CourseSection {
    private int sectionID;
    private int capacity;
    private int seatsLeft;

    private Course course;
    private Quarter quarter;
    private ArrayList<Student> roster;  //list of enrolled students

    final Logger logger = Logger.getLogger(CourseSection.class.getName());

    // Constructor
    public CourseSection(Course course, Quarter quarter) {
        roster = new ArrayList<>();
        this.course = course;
        this.quarter = quarter;
    }

    //Each section has an ID
    public void setSectionID(int sectionID) {
        this.sectionID = sectionID;
    }

    public int getSectionID() {
        return sectionID;
    }

    public Course getCourse() {
        return course;
    }

    //Each section has a capacity, value of which seatsLeft initially matches
    public void setCapacity(int seats) {
        capacity = seats;
        seatsLeft = seats;
    }

    public Quarter getQuarter()
    {
        return this.quarter;

    }
    public int getCapacity() {
        return capacity;
    }

    public int getSeatsLeft(){
        return seatsLeft;
    }

    // List of students
    public ArrayList<Student> getRoster() {
        return this.roster;
    }

    public int getCourseSectionSize() {
        return roster.size();
    }

    // Accepts student into the roster, based on student eligibility, roster size, deadline
    public boolean enroll(Student student) throws SQLException {
        if (!student.canRegisterCourse(course) ||this.getCapacity() == this.getRoster().size() || Calendar.getInstance().getTime().after(quarter.getDeadline())) {
            sendEmail("Please review tentative enrollment for Student " + student.getID());
            return false;
        }
        else {
            roster.add(student);
            student.insertTakesCourse(this.getCourse(),student);
            seatsLeft--;
            return true;
        }
    }

    // Associates student's grade in the section's course
    public void setGrade(Student student, String grade) {
        student.addGrade(this.course, grade);
    }

    public void sendEmail(String txt)

    {
        logger.info("Mail Sent to this Course's instructor: " + txt);
    }

    //Add and drop methods to the catalog
    public void addToCatalog(){
        //...

        //CALL INSERT
        System.out.println("Course section added to catalog");
    }

    public void drop() {

        //CALL DELETE OR MAKE IT HAPPEN HERE
        System.out.println("Course section removed from catalog");
    }

}