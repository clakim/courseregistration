/**
 * User is the abstract base class of Student, Instructor, and Administrator.
 * All users have a first and last name, id, and e-mail address. All can sendEmail in their own implementation
 * (functionality of which is not yet fleshed out in this practicum).
 */

public abstract class User {

    String firstName, lastName;
    int id;
    String email;


    public User(String firstName, String lastName, int id, String email)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.email = email;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public int getID(){
        return id;
    }

    public String getEmail(){
        return email;
    }

}