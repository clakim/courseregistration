/**
 * An AcademicAdvisor is a specialized Staff member that advises multiple students and receives emails in response to registration actions.
 */

import java.util.ArrayList;

public class AcademicAdvisor extends Staff implements Observer{

    private ArrayList<Student> studentsAdvised = new ArrayList<>();

    public AcademicAdvisor(String firstName, String lastName, int id, String email, String employmentStatus){
        super(firstName, lastName, id, email, employmentStatus);
    }
    public void setStudentsAdvised(ArrayList<Student> studentsAdvised) {
        this.studentsAdvised = studentsAdvised;
    }

    public void addStudentsAdvised(Student s) {
        studentsAdvised.add(s);
    }

    public ArrayList<Student> getStudentsAdvised() {
        return studentsAdvised;
    }

    // Can be updated of news of course as per the Observer pattern
    public void update(String catalogInclusion, int courseID) {
        System.out.println("Email to Academic Advisor " + this.getID() + ": Course " + courseID + " is now "+ catalogInclusion);
    }
}
