/**
 * A Staff object (member) is a specialized User that works a non-teaching job at the University. It is a superclass of AcademicAdvisor.
 */

public class Staff extends User {

    private String employmentStatus;
    private String department;

    public Staff(String firstName, String lastName, int id, String email, String employmentStatus){
        super(firstName, lastName, id, email);
        this.employmentStatus = employmentStatus;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

}
