/*
 * Students having TentativeEnrollmentState either must receive instructor's permission or have requested enrollment after all seats have been taken.
 */

import java.sql.SQLException;

public class TentativeEnrollmentState implements EnrollmentState {
    public void doAction(Student s, Course course) throws SQLException {
        System.out.println("In Tentative enrollment state");
        s.setEnrollmentState(this);

        // Add to waitlist
        s.insertStudentToWaitlist(course, s);

        // Send email to instructor for permission
        s.sendEmail("Instructor " + course.getInstructor().getFirstName() + " " + course.getInstructor().getLastName(),
                "Requesting permission for course " + course.getCourseID());


        if (course.getInstructor().approveStuBackground(s, course)){
            s.setEnrollmentState(new EnrolledEnrollmentState());
        }
    }

    public String toString() {
        return "Tentative Enrollment State";
    }
}

