import java.sql.SQLException;

public class EnrolledEnrollmentState implements EnrollmentState {
    public void doAction(Student s, Course course) throws SQLException {

        // add student to roster
        s.insertTakesCourse(course, s);

        System.out.println("In Enrolled enrollment state");
        s.setEnrollmentState(this);
    }
    public String toString() {
        return "Enrolled Enrollment State";
    }
}
