/*
 * Observer interface that abstracts the updating of the observers with news
 */

public interface Observer {
    public void update(String catalogRemovedNote, int courseID);
}