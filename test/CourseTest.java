import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import static junit.framework.TestCase.*;

public class CourseTest {
    Course c1;
    Department d1;
    DeptChair dc;
    Course c2;
    ArrayList<Course> coursePrereqs = new ArrayList<>();
    ArrayList<CourseSection> sections;

    @Before
    public void init() throws Exception {
        System.out.println("Setting up ...");
        ArrayList<CourseSection> coursesections = new ArrayList<CourseSection>();
        dc = new DeptChair("Harry", "Smith", 52, "hsmith@dc.edu", "ARCH", coursesections, "Chair");
        d1 = new Department("Architecture", dc);
        c1 = new Course(300, "Urban Planning", d1, 3);
        c2 = new Course(305, "Intellect of Entelechy", d1, dc, 4, true, coursePrereqs);
    }

    @After
    public void destroy() throws Exception {
        System.out.println("Tearing down ...");
        c1 = null;
    }

    @Test
    public void testReqsInstrPermission() {
        //Assert.assertNotNull(c2); // should not pass
        c2 = new Course(305, "Intellect of Entelechy", d1, dc, 4, true, coursePrereqs);
        //assertFalse(c2.reqsInstrPermission());      //should not pass
    }

    @Test
    public void testRegisterandGetObservers() {
        Student s4 = new Student("Ellie", "Hogeman", 123, "concreteart@uchicago.edu");
        c2.registerObserver(s4);
        assertNotNull(c2.getObservers().get(0)); //passes; does not pass when above line commented out
    }

    @Test
    public void testAddSection() {
        Quarter q1 = new Quarter(1802, "Spring 2018", Date.valueOf("2018-03-26"));
        c2.createSections(2, q1);
        assertEquals(2, c2.getSections().size());

    }

    @Test
    public void testGetCumulativeCount() throws SQLException {

        Quarter q1 = new Quarter(1802, "Spring 2018", Date.valueOf("2018-03-26"));
        c2.createSections(2, q1);
        System.out.println(c2.getSections().get(0).getCourseSectionSize());
        System.out.println(c2.getSections().get(1).getCourseSectionSize());
        Student s5 = new Student("A", "Smith", 1236, "y@y.com");
        c2.getSections().get(0).enroll(s5);
//      assertEquals(1, c2.getCumulativeCount()); // Correctly does not pass because there is no indication that
// student is eligible to enroll
    }
}