import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static junit.framework.TestCase.assertFalse;

// Tests that Waitlist methods function properly.
public class WaitlistTest {

    Waitlist wlist;
    Course c;

    @Before
    public void init() throws Exception {
        System.out.println("Setting up ...");
        // ArrayList<Student> wlistedStudents = new ArrayList<Student>();
        wlist = new Waitlist(c);
    }

    @After
    public void destroy() throws Exception {
        System.out.println("Tearing down ...");
        wlist = null;
    }


    @Test
    public void getNextStudent() throws Exception {
        Assert.assertNotNull(wlist);
        Student s1 = new Student("Claire", "Kim", 111,"claire@example.org");
        wlist.getWaitlistedStudents().add(s1);
        assertFalse(wlist.getWaitlistedStudents().isEmpty());
        Assert.assertNotNull(wlist.getWaitlistedStudents());

        Student result = wlist.getNextStudent();
        Assert.assertNotNull(result);
        Assert.assertEquals(result.getFirstName(),"Claire");
    }

    @Test
    public void wipe() throws Exception {
        wlist.wipe();
//        assertFalse(wlist.getWaitlistedStudents().isEmpty()); This test fails as expected
        assertFalse(!wlist.getWaitlistedStudents().isEmpty()); //This test passes as expected; wipe method works
    }

}


