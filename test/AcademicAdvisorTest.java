import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AcademicAdvisorTest {

    AcademicAdvisor aadvisor;

    @Before
    public void init() throws Exception {
        System.out.println("Setting up ...");
        aadvisor = new AcademicAdvisor("Julie", "Penn", 20, "iadvisestudents1@uchicago.edu","FT");
    }

    @After
    public void destroy() throws Exception {
        System.out.println("Tearing down ...");
        aadvisor = null;
    }

    @Test
    public void getStudentsAdvised() throws Exception {
        Student s1 = new Student("Claire", "Kim", 111,"claire@example.org");
        aadvisor.addStudentsAdvised(s1);
        System.out.println(aadvisor.getStudentsAdvised().get(0).getFirstName());
        Assert.assertNotEquals(aadvisor.getStudentsAdvised().get(0).getFirstName(),"Mary");
    }
}