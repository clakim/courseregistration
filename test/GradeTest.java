import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

// Tests that Grade methods handle W and I properly.
public class GradeTest {
    Grade grade1, grade2;

    @Before
    public void init() throws Exception {
        System.out.println("Setting up ...");
        grade1 = new Grade("W");
        grade2 = new Grade("I");
    }

    @After
    public void destroy() throws Exception {
        System.out.println("Tearing down ...");
        grade1 = null;
        grade2 = null;
    }

    @Test
    public void testGradeAsNum() {
        Assert.assertNotNull(grade1);
        Assert.assertNotNull(grade2);
        double grade1Test = grade1.getNumGrade();
        Assert.assertNotEquals(grade1Test,2);

        Department physDept = new Department("PHYS");
        Department tapsDept = new Department("TAPS");

        Student s1 = new Student("Claire", "Kim", 111,"claire@example.org");
        Course c1 = new Course(102, "Blowing Bubbles Cubically", physDept, 1);
        Course c2 = new Course(104, "Speed-Breathing", tapsDept, 1);
        s1.addGrade(c1,"A");
        s1.addGrade(c2,"B+");
        s1.addGrade(c2,grade2.toString());

        System.out.println(s1.getTranscript().getGrades());
        System.out.println(s1.getTranscript().getGrades().values());
        System.out.println("Student GPA = " + s1.getGPA()); //shows that incomplete grade "I" yields 0.0 numerical value, will fix in final
    }
}