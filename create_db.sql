USE reg_system;


DROP TABLE IF EXISTS TakesCourse;
DROP TABLE IF EXISTS Transcript;
DROP TABLE IF EXISTS Grade;
DROP table if exists StudentsWaitlisted;
DROP table if exists Waitlist;
DROP table if exists CourseSection;
DROP TABLE IF EXISTS Quarter;
DROP TABLE IF EXISTS Course;
DROP TABLE IF EXISTS DeptChair;
DROP TABLE IF EXISTS Faculty;
DROP TABLE IF EXISTS Instructor;
DROP TABLE IF EXISTS AcademicAdvisor;
DROP TABLE IF EXISTS Student;
DROP TABLE IF EXISTS User;
 

CREATE TABLE User
(
  userID INTEGER NOT NULL,
  firstName VARCHAR(40),
  lastName VARCHAR(40),
  email VARCHAR(40),
  PRIMARY KEY(userID)
);

CREATE TABLE AcademicAdvisor
(
  acadvisorID INTEGER,
  employmentStatus VARCHAR(2),  
  PRIMARY KEY (acadvisorID),
  FOREIGN KEY (acadvisorID) REFERENCES User (userID)
);

CREATE TABLE Instructor
(
  instructorID INTEGER NOT NULL,
  department VARCHAR(4),
  sectionTaught VARCHAR(8),
  PRIMARY KEY (instructorID, sectionTaught),
  FOREIGN KEY (instructorID) REFERENCES User (userID)
);

CREATE TABLE Faculty
(
  facultyID INTEGER NOT NULL,
  title VARCHAR(15),
  PRIMARY KEY (facultyID)
#  FOREIGN KEY (facultyID) REFERENCES Instructor (instructorID)
);

CREATE TABLE DeptChair
(
  chairID INTEGER NOT NULL, 
  numCases INTEGER,
  PRIMARY KEY (chairID),
  FOREIGN KEY (chairID) REFERENCES Faculty (facultyID)
);

CREATE TABLE Course
(
  courseID SMALLINT,
  courseCatNum INTEGER,
  courseName VARCHAR(50),
  department VARCHAR(20),
  facultyID INTEGER,
  numCredits INTEGER,
  prereqCourseID INTEGER,	
  needsInstrPerm BOOLEAN,
  PRIMARY KEY (courseID),
  FOREIGN KEY (facultyID) REFERENCES Faculty (facultyID)
);

CREATE TABLE Quarter
(
  quarterID INTEGER,
  quarterName VARCHAR(30), 
  startDate DATE,
  deadline DATE, 
  endDate DATE,
  PRIMARY KEY (quarterID)
);

CREATE TABLE CourseSection
(
	courseSectionID SMALLINT, 
    capacity INTEGER, 
    seatsLeft INTEGER, 
    courseID SMALLINT, 
    quarterID INTEGER,
    PRIMARY KEY (courseSectionID, courseID),
	FOREIGN KEY (courseID) REFERENCES Course (courseID),
	FOREIGN KEY (quarterID) REFERENCES Quarter (quarterID)
);

CREATE TABLE Waitlist
(
	waitlistID INTEGER, 
    courseID SMALLINT, 
    quarterID INTEGER,
    PRIMARY KEY (waitlistID),
	FOREIGN KEY (courseID) REFERENCES Course (courseID),
	FOREIGN KEY (quarterID) REFERENCES Quarter (quarterID)
);

CREATE TABLE Student
(
  studentID INTEGER NOT NULL,
  enrollStatus VARCHAR(2),
  major VARCHAR(4),
  year TINYINT,
  currentBalance FLOAT(7,2),
  courseLoad TINYINT,
  courseLimit TINYINT,
  meetsGPAPrereq BOOLEAN,
  meetsLibraryPrereq BOOLEAN,
  meetsFormsPrereq BOOLEAN,
  meetsFeesPrereq BOOLEAN,
  acadvisorID INTEGER,
  transcriptID INTEGER,
  gpa FLOAT(2,1),
  PRIMARY KEY(studentID),
  FOREIGN KEY (studentID) REFERENCES User (userID)
#  FOREIGN KEY (acadvisorID) REFERENCES AcademicAdvisor (acadvisorID),
#  FOREIGN KEY (transcriptID) REFERENCES Transcript (transcriptID)
); 


CREATE TABLE StudentsWaitlisted
(
	waitlistID INTEGER, 
    studentID INTEGER, 
    timeStamp TIMESTAMP,  
    PRIMARY KEY (waitlistID, studentID),
	FOREIGN KEY (waitlistID) REFERENCES Waitlist (waitlistID),
	FOREIGN KEY (studentID) REFERENCES Student (studentID)
);


CREATE TABLE Grade
(
  studentID INTEGER,
  courseID INTEGER, 
  sectionID INTEGER,
  quarterID INTEGER,
  grade VARCHAR(2),
  PRIMARY KEY (studentID, courseID, quarterID),
  FOREIGN KEY (studentID) REFERENCES Student (studentID)
);

CREATE TABLE Transcript
(
  transcriptID INTEGER,
  courseID smallint,  
  quarterID INTEGER,
  grade VARCHAR(2),
  PRIMARY KEY (transcriptID, courseID, quarterID),
  FOREIGN KEY (courseID) REFERENCES Course (courseID),
  FOREIGN KEY (quarterID) REFERENCES Quarter (quarterID)
);

CREATE TABLE TakesCourse
(
  studentID INTEGER,
  courseID smallint,  
  quarterID INTEGER, 
  PRIMARY KEY (studentID, courseID, quarterID),
  FOREIGN KEY (studentID) REFERENCES Student (studentID),
  FOREIGN KEY (courseID) REFERENCES Course (courseID),
  FOREIGN KEY (quarterID) REFERENCES Quarter (quarterID)
);


CREATE TABLE UserPassword
(
  userID INTEGER,
  userName VARCHAR(2),  
  userPassword VARCHAR(2), 
  userLoginCategory VARCHAR(20),
  PRIMARY KEY (userID),
  FOREIGN KEY (userID) REFERENCES User (userID)
);



 