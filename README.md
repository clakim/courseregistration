README.md  

Summary: Exhibition of the bounded contexts of permission and notification, centered around the two major events of 1) addability of a student to the course roster and 2) addability of a course to the catalog.  

"Permissions," "Enrollments," and "Communications" are bounded contexts including these functionalities:  
1. Allowing a student to enroll based on permissions criteria such as academic history, student enrollment status (full-time, part-time, any-time), course availability (is the course being offered, and if so, is it full?), and  
2. Allowing a course to be enrollable based on student enrollment (class size of 5 being a tipping point), with all relevant communication between the users.

Final Project Content:  
1. Code of all classes, tests  
2. 13 related MySQL tables populated with data  
3. Driver Program allows command-line interface interactions as Student, FacultyMember, or Administrator, simulating a finite sampling of scenarios.

Content In Detail:

1. Code pattern design and intentions  
(Compile and run as typical for Java)  
I implemented the Singleton pattern for the JDBC database connection, the Observer pattern for notification of a course being dropped (where Course implements the Subject Interface, and Observers are the students enrolled, academic advisors, and faculty of the course), and two usages of the State pattern - one for the state of a Course (ListingState, or listedness: 3 states being Proposed, Open, Closed), and the other for the state of a Student (PendingEnrollment, TentativeEnrollment, EnrolledEnrollment, DeniedEnrollment).

2. To load the tables, you can copy and paste the contents of create_db.sql into the command line, then populate_db.sql. For relative convenience I did not auto-increment columns that would typically be benefited by this (User table.)

3. Driver program, ProgramRunner:  
Extracts methods implementation and/or suggests the procedure from the other coded classes.

Guide:  
The tester can follow the prompts to assume the role of Student, Faculty, or Administrator in a variety of situations.  
Scenarios, excluding quit -
    Student:  
    1. Try to enroll in a course, generic  
    2. Try to enroll in a Course known to be at capacity (corresponds to Student - EnrollmentState - TentativeState). (Course ID1 has 6 spots and all are filled)  
    3. Try to enroll in a Course that requires instructorPermission (corresponds to Student - EnrollmentState - TentativeState)  
    4. Try to enroll in a Course when you are already at your limit. (corresponds to Student - EnrollmentState - PendingState)  
    5. Try to enroll in a Course that will end up not having enough students (EnrollmentState - EnrolledEnrollmentState)  
    6. Try to enroll in a Course after the deadline has passed. (Course - ListingState - ClosedListingState)  
    7. Try to enroll in a course that has a prerequisite that you MIGHT not have in your course history. (Case for might not: if you’re student 2 and want to take course 7).

    Faculty:  
    1. Review the students in your waitlist for a course.  
    2. Add this student on Waitlist or Tentative Enrollment to your roster.  
    3. Give a grade to a student in the class you are teaching.  

    Administrator: (the user with id 1)  
    1. Fish for courses that have less than 5 students (Course - Listing State - ClosedListingState)  
    2. Manually update the prerequisites of a course (must be during Course - ListingState = ProposedListingState)
